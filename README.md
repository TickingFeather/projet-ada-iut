# Anti-Virus
Projet de programmation Ada, fin de 1er semestre de DUT Info à l'IUT2 Grenoble.  
Recréation du jeu de puzzle *[Anti-Virus](https://www.smartgames.eu/fr/jeux-pour-1-joueur/anti-virus)*.

## Build
Dépendances :
- GNAT et `gnatmake` (GNAT8 ciblé, fonctionne peut-être avec version plus récente)
- `libX11`, `libXt`, `libforms` pour la version graphique

Compiler la version CLI (ligne de commande) :
```
./build.sh
```
Compiler la version graphique :
```
./build.sh graph
```
Nettoyer les fichiers compilés :
```
./build.sh clean
```

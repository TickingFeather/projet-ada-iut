with text_io,p_virus,p_esiut; use text_io,p_virus,p_esiut;

package p_vuetxt is

  package p_int_io is new integer_io(integer); use p_int_io;

  procedure AfficheGrille(Grille: in TV_Grille);
  --{} => {la grille a été affichée selon les spécifications suivantes :
  --*la sortie est indiquée par la lettre S
  --*une case inactive ne contient aucun caractère
  --*une case de couleur vide contient un point
  --*une case de couleur blanche contient le caractère F (Fixe)
  --*une case de la couleur d’une pièce mobile contient le chiffre correspondant à la
  --position de cette couleur dans le type T_Coul}

  procedure AffichePossib(Grille : in TV_Grille ; Pieces : in TV_Pieces);
  --{Grille configurée} => {Les déplacements possibles sont affichés}

  procedure Accueil(f : in out text_io.file_type);
  --{f ouvert, correspond au fichier de paramètres} => {l'écran titre a été affiché,
  --ainsi que l'aide en fonction des paramètres du fichier. Si fichier invalide, EX_PARAM_INVALIDE a été déclenchée}
end p_vuetxt;

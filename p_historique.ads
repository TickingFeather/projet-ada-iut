with p_virus,p_vuetxt; use p_virus,p_vuetxt;

package p_historique is

  -- représente un déplacement : couleur et direction
  type TR_Dep is record
    couleur: T_CoulP;
    dir : T_Direction;
  end record;

  --représente l'historique : vecteur de déplacements
  type TV_Hist is array(positive range <>) of TR_Dep;

  EX_HIST_VIDE : exception;

  function histoVide(h : in TV_Hist; itop : in natural) return boolean;
  --{itop est l'indice du dernier élément défini de h} => {résultat : true si historique vide}

  procedure empilerCoup(h : in out TV_Hist; itop : in out natural; val : in TR_Dep);
  --{itop est l'indice du dernier élément défini de h} => {val a été ajoutée à l'historique, qui a été reculé
  -- d'un cran (1re valeur oubliée) SI il était plein,et itop est l'indice du sommet de la pile}

  procedure depilerCoup(h : in out TV_Hist; itop : in out natural; val : out TR_Dep);
  --{itop est l'indice du dernier élément défini de h} => {si l'historique est vide, EX_PILE_VIDE est déclenchée, sinon le dernier élément
  --de l'historique a été enlevé, val contient sa valeur, itop est l'indice du dernier élément}
end p_historique;

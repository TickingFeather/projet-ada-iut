#!/usr/bin/env bash

# A COMPLÉTER avec l'emplacement de votre répertoire 'bib' contenant 'lib' et 'x11ada'
# ( sans / à la fin )
ADA_BIB="./bib"

if [ "$1" = "clean" ]; then
	rm -f *.ali *.o av_txt av_graph
	exit 0
elif [ "$1" = "graph" ]; then
	executable="av_graph"
	# Variables d'environnement pour indiquer les emplacements des bibliothèques
	ADAX_PATH="$ADA_BIB/x11ada" # libs X11
	ADA_FORMS="$ADA_BIB/lib" # lib forms
	export ADA_OBJECTS_PATH="$ADAX_PATH:$ADA_OBJECTS_PATH"
	export ADA_INCLUDE_PATH="$ADAX_PATH:$ADA_BIB:$ADA_INCLUDE_PATH"
	export LD_LIBRARY_PATH="$ADA_FORMS"
	# Arguments du linker
	#LARGS="-L/usr/X11R6/lib -largs -lforms -largs ${ADAX_PATH}/var.o -largs -lX11 -largs -lXm -largs -lXt"
	LARGS="-L$ADA_FORMS -largs -lforms -largs ${ADAX_PATH}/var.o -largs -lX11 -largs -lXt"
	# Compilation
	gnatmake -gnatv -gnato $executable $LARGS && echo -e "\nN'oubliez pas de 'export LD_LIBRARY_PATH=\"$LD_LIBRARY_PATH\"' avant de lancer l'exécutable"

else
	executable="av_txt"
	export ADA_INCLUDE_PATH="$ADA_BIB:$ADA_INCLUDE_PATH"
	gnatmake -gnatv -gnato $executable
fi

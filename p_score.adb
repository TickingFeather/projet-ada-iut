package body p_score is

  function nbElem(f : in p_score_io.file_type) return natural is
    --{f ouvert, f = <>} => {résultat : nombre d'éléments dans f}
    elem : TR_Score;
    cpt : natural := 0;
  begin -- nbElem
    while not end_of_file(f) loop
      read(f,elem);
      cpt := cpt+1;
    end loop;
    return cpt;
  end nbElem;

  function NbScoreConfig (f : in p_score_io.file_type;  nbconfig : in positive) return natural is
    --{f ouvert, f-=<>} => {resultat : nombre de score enregistrés pour la configuration nbconfig}
    cpt : natural := 0;
    score : TR_Score;
  begin -- NbScoreConfig
    while not end_of_file(f) loop
      read(f,score);
      -- Si le score lu a été réalisé sur la configuration cherchée
      if score.nbconfig=nbconfig then
        cpt := cpt+1;
      end if;
    end loop;
    return cpt;
  end NbScoreConfig;

  function getScoreConfig (f : in p_score_io.file_type;  nbconfig : in positive; nbElemConf : in natural) return TV_Score is
    -- {f ouvert, f- = <>} => {resultat = TV_Score qui contient les scores pour la configuration nbconfig}
    V : TV_Score(1..nbElemConf);
    cpt : natural := V'first; --Indice incrémentiel des éléments du vecteur
    score : TR_Score;
  begin -- getScoreConfig
    while not end_of_file(f) loop
      read(f,score);
      -- Si le score lu a été réalisé sur la configuration cherchée
      if score.nbconfig=nbconfig then
        V(cpt) := score;
        cpt := cpt+1; -- Le prochain élément trouvé sera mis à la suite
      end if;
    end loop;
    return V;
  end getScoreConfig;

  function InfStrict(S1,S2 : TR_Score) return boolean is
    -- {} => {vrai si S1 est strictement inférieur à S2 selon l'ordre (temps,coups)}
  begin
    return (S1.temps < S2.temps or (S1.temps = S2.temps and S1.nbcoup < S2.nbcoup));
  end InfStrict;

  procedure AjouterScore (f :  in out p_score_io.file_type; nbconfig : in positive; pseudo : String; nbCoup : positive; temps : float) is
    -- {f ouvert} => {Le score a été inséré dans f, selon l'ordre (coup,temps)}
    scoreAInsert : TR_Score := (nbconfig,pseudo,nbcoup,temps);
    score : TR_Score;
    cpt : positive := 1;
  begin
    declare
      V : TV_Score(1..nbElem(f)+1); --Vecteur avec un élément de plus que le nombre d'éléments du fichier pour pouvoir insérer le score
      i : integer := V'first;
    begin
      reset(f,in_file);
      --Si le fichier est vide
      if end_of_file(f) then
        reset(f,out_file);
        write(f,scoreAInsert); --Simplement écrire le score
      else
        --S'il y a au moins un score
        while not end_of_file(f) loop
          read(f,score);
          V(cpt) := score; --Recopier les éléments du fichier dans le vecteur
          cpt := cpt+1;
        end loop;

        --On avance dans le vecteur jusqu'à trouver l'emplacement pour trier selon l'ordre (coup,temps)
        while i < V'last and then InfStrict(V(i),scoreAInsert) loop
          i := i+1;
        end loop;
        --Si le score à entrer est le dernier
        if i = V'last then
          V(i) := scoreAInsert;
        else
          --Sinon, décaler le vecteur et insérer le score
          V(i+1..V'last) := V(i..V'last-1);
          V(i) := scoreAInsert;
        end if;

        reset(f,out_file);

        for i in V'range loop
          write(f,V(i)); --Réécrire le fichier depuis le vecteur
        end loop;
      end if;

    end;
    reset(f,in_file);
  end AjouterScore;

end p_score;

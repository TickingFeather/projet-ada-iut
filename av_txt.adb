with p_virus,p_vuetxt,p_esiut,text_io; use p_virus,p_virus.p_piece_io,p_vuetxt,p_esiut,text_io;
use p_virus.p_coul_io,p_virus.p_dir_io,p_virus.p_int_io;
with p_historique; use p_historique;

procedure av_txt is
  --{} => {}
  numconfig : positive := 1;
  grille : TV_Grille;
  pieces : TV_Pieces;
  f : p_piece_io.file_type;
  fparam : text_io.file_type;
  confirm : character := '#';
  choix : string(1..10);
  coulSelect : T_CoulP ;
  dirSelect : T_Direction;
  fin : natural;
  cptcoup : positive :=1 ;-- compteur de coups

  his : TV_Hist(1..50) := (others => (blanc,hg));--représentent l'historique..
  itop : natural := his'first-1;--                .. des coups précédents
  dep : TR_Dep;-- représente un élément de l'historique, càd une couleur et une direction
begin -- av_txt

  InitPartie(grille,pieces);-- Initialisation de la grille vide
  open(f,in_file,"Parties");-- On ouvre le fichier de parties

  begin
    open(fparam,in_file,"AVsettings.txt");
  exception
    when text_io.NAME_ERROR => create(fparam,out_file,"AVsettings.txt");
  end;
  --=============================== MISE EN PLACE ================================
  Accueil(fparam);

  loop
    loop-- Saisie numéro de config, avec contrôle
      begin
        put("Entrez un numéro de niveau (1-20) : ");
        Lire(numconfig);
        exit when numconfig in 1..20;
        put_line("Ce numéro n'existe pas.");-- Affiché seulement si condition du exit when non remplie
      exception
        --Traitement par exception pour valeurs < 1
        when CONSTRAINT_ERROR => put_line("Ce numéro n'existe pas.");
      end;
    end loop;

    Configurer(f,numconfig,grille,pieces);-- Placement des pièces
    new_line;
    put_line("-- Aperçu de la grille --");new_line;
    AfficheGrille(grille);-- Affichage grille comme aperçu de configuration
    new_line;
    put("Voulez-vous jouer sur ce niveau ? (o/n) : ");
    Lire(confirm);
    new_line;
    exit when confirm = 'o'  or confirm = 'O';
  end loop;
--================ DEBUT PARTIE ========================
  clr_ecran;
  new_line;
  while not Guerison(Grille) loop-- boucle principale des tours de jeu

    loop

      loop-- boucle de saisie controlée de la couleur
        begin-- begin nécessaire pour traitement des exceptions dans la boucle

          new_line;
          put(" ----- Coup n°");
          put(cptcoup,0);
          put_line(" -----");
          new_line;

          AfficheGrille(Grille);
          put_line("Possibilités de déplacements :");
          AffichePossib(grille,pieces);-- Affichage déplacements possibles pour chaque couleur

          loop
            put("Saisissez une couleur à déplacer");
            if not histoVide(his,itop) then
              put(" (ou 'a' pour annuler le coup précédent)");
            end if;
            put(" : ");
            get_line(choix,fin);
          exit when (choix(1..fin) = "q" or choix(1..fin) = "a") or else (Pieces(T_coulP'value(choix(1..fin))) and T_coulP'value(choix(1..fin)) /= blanc);
            raise EX_CHOIX_COUL_INVALIDE;
          end loop;
          new_line;
          if choix(1..fin)="a" then
            -- ANNULATION COUP
            depilerCoup(his,itop,dep);--                    On retire un déplacement de l'historique,
            MajGrille(grille,dep.couleur,invDir(dep.dir));-- on l'applique en inversant la direction,
            cptcoup := cptcoup-1;--                           et on décrémente le compteur de coups

            clr_ecran;
            -- Message de confirmation
            put_line("Annulation du coup précédent : pièce " & T_coulP'image(dep.couleur) & " déplacée dans la direction " & T_Direction'image(invDir(dep.dir)) & ".");

          elsif choix(1..fin)="q" then
            raise EX_FIN_PARTIE;
          else
            coulSelect := T_CoulP'value(choix(1..fin));
            exit when PieceMobile(grille,coulSelect);
            clr_ecran;
            put_line("Impossible de déplacer cette pièce, veuillez recommencer.");
          end if;

        exception
          when CONSTRAINT_ERROR => clr_ecran; put_line("Choix invalide, recommencez.");
          when EX_HIST_VIDE => clr_ecran; put_line("Historique vide, impossible d'annuler le dernier coup.");
          when EX_FIN_PARTIE => raise EX_FIN_PARTIE;
          when EX_CHOIX_COUL_INVALIDE => clr_ecran; put_line("La couleur saisie n'est pas en jeu et/ou ne peut pas être déplacée.");
        end;
        -- skip_line;
      end loop;-- fin boucle saisie contrôlée de la couleur



      put("Dans quelle direction déplacer la pièce " & T_coulP'image(coulSelect) & " ? ");
      lire(dirSelect);


      exit when Possible(Grille,coulSelect,dirSelect);
      clr_ecran;
      put_line("Déplacement impossible, veuillez recommencer.");
    end loop;
    -- loop
    --   put("Saisissez une couleur à déplacer : ");
    --   lire(coulSelect);
    --
    --
    --   put("Dans quelle direction déplacer la pièce " & T_coulP'image(coulSelect) & " ? ");
    --   Lire(dirSelect);
    --
    --
    --   exit when Possible(Grille,coulSelect,dirSelect);
    --   put_line("Déplacement impossible, veuillez recommencer.");
    -- end loop;
    MajGrille(Grille,coulSelect,dirSelect);
    -- Stocker le coup dans l'historique
    dep := (coulSelect,dirSelect);
    empilerCoup(his,itop,dep);
    -- Incrémenter compteur de coups
    cptcoup := cptcoup+1;
    -- new_line(40);
    clr_ecran;
    put_line("Pièce " & T_coulP'image(coulSelect) & " déplacée dans la direction " & T_Direction'image(dirSelect) & ".");
  end loop;

  --Victoire

  new_line;
  put_line(" ==== Victoire !! ==== ");
  new_line;
  AfficheGrille(grille);
  new_line;
  put("Vous avez éliminé le virus en ");
  put(cptcoup-1,0);put_line(" coups !");-- Fin détectée au début du coup N, donc victoire au coup N-1
exception
  when EX_PARAM_INVALIDE =>
    put_line("Erreur dans le fichier de paramètres, effacement automatique...");
    reset(f,out_file);
  when EX_FIN_PARTIE =>
    new_line;
    put_line(" ==== Partie annulée... ==== ");
end av_txt;

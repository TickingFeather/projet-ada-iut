package body p_vuetxt is

  procedure AfficheGrille(Grille: in TV_Grille) is
  --{} => {la grille a été affichée selon les spécifications suivantes :
  --*la sortie estindiquée par la lettre S
  --*une case inactive ne contient aucun caractère
  --*une case de couleur vide contient un point
  --*une case de couleur blanche contient le caractère F (Fixe)
  --*une case de la couleur d’une pièce mobile contient le chiffre correspondant à la
  --position de cettecouleur dans le type T_Coul}

  begin -- AfficheGrille
    put_line("    A B C D E F G");
    put_line("  S - - - - - - -");

    for l in Grille'range(1) loop--   Pour chaque case..
      put(l,0); put(" | ");
      for c in Grille'range(2) loop-- de la grille (ligne, colonne)
        if (l mod 2) /= (character'pos(c) mod 2) then
          put(" ");
        else
          case Grille(l,c) is
            when vide => put("•");
            when blanc => put('F');
            when others => put(T_coulP'pos(Grille(l,c)),0);
          end case;
        end if;

        put(" ");
      end loop; -- Fin colonne
      new_line;
    end loop;   -- Fin ligne
    new_line;
  end AfficheGrille;

  procedure AffichePossib(Grille : in TV_Grille; Pieces : in TV_Pieces) is
    --{Grille configurée} => {Les déplacements possibles sont affichés}

  begin -- AffichePossib
    for couleur in T_coulP loop-- Pour chaque couleur
      if Pieces(couleur) and couleur /= blanc then-- mais seulement celles présentes en jeu
        --Afficher la couleur et son numéro
        put(T_coulP'image(couleur) & " (");
        put(T_coulP'pos(couleur),0);
        put(") : ");

        put("  ");
        for dir in T_Direction loop -- Pour chaque direction
          if Possible(grille,couleur,dir) then  -- Si cette direction est possible
            -- Afficher la direction disponible
            put(T_Direction'image(dir) & " ");
          end if;
        end loop;
        new_line;
      end if;
    end loop;
    new_line;
  end AffichePossib;



  procedure Accueil(f : in out text_io.file_type) is
    --{f ouvert, correspond au fichier de paramètres} => {l'écran titre a été affiché,
    --ainsi que l'aide en fonction des paramètres du fichier. Si fichier invalide, EX_PARAM_INVALIDE a été déclenchée}
    choix : string(1..20);
    aide : character;
    fin : natural;
  begin -- Accueil
    clr_ecran;
    put_line("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
    put_line("                   _   _           __      ___                ");
    put_line("       /\         | | (_)          \ \    / (_)               ");
    put_line("      /  \   _ __ | |_ _   ______   \ \  / / _ _ __ _   _ ___ ");
    put_line("     / /\ \ | '_ \| __| | |______|   \ \/ / | | '__| | | / __|");
    put_line("    / ____ \| | | | |_| |             \  /  | | |  | |_| \__ \");
    put_line("   /_/    \_\_| |_|\__|_|              \/   |_|_|   \__,_|___/");
    new_line;
    put_line("- - - - - - - - Appuyez sur Entrée pour continuer - - - - - - -");
    skip_line;

    reset(f,in_file);
    if end_of_file(f) then-- si fichier vide (tout juste créé), on l'initialise
      reset(f,out_file);
      put(f,"1 => Affichage de l'aide (0 / 1)");-- par défaut : aide affichée
      reset(f,in_file);
    end if;

    get(f,aide);
    if aide = '1' then
      put_line("Un virus s'est introduit dans l'une de vos cellules. Pourrez-vous");
      put_line("vous en débarrasser avant qu'il ne se soit multiplié ?");
      put_line("Alors que la cellule (la grille de jeu) contient peu de place,");
      put_line("vous allez devoir gérer le déplacement des molécules");
      put_line("(pièces de couleur)  qui se gênent pour en extraire le virus (pièce rouge).");
      new_line;
      put_line("20 niveaux, de difficultés croissantes, sont disponibles.");
      put_line("Pour chaque niveau, le but est de faire parvenir la pièce rouge");
      put_line("dans le coin supérieur gauche en un minimum de coups.");
      put_line("À chaque tour, vous devez entrer la couleur de la pièce à déplacer,");
      put_line("puis la direction diagonale de déplacement : ");
      put_line("  - en haut à gauche (HG)");
      put_line("  - en haut à droite (HD)");
      put_line("  - en bas à gauche (BG)");
      put_line("  - en bas à droite (BD)");
      new_line;
      put_line("La casse des couleurs et déplacements est ignorée.");
      put_line("Vous pouvez annuler vos coups précédents en entrant 'a' au lieu");
      put_line("d'une couleur, sauf si l'historique de coups est vide");
      put_line("(au 1er tour par exemple).");
      put_line("Vous pouvez également quitter en entrant 'q' au lieu d'une");
      put_line("couleur.");
      new_line;

      loop
        put("Voulez-vous afficher cette aide la prochaine fois ? (o/n) : ");
        get_line(choix,fin);
        exit when choix(1..fin)="o" or choix(1..fin)="n";
        put_line("Veuillez saisir 'o' ou 'n'.");
      end loop;
      if choix(1)='n' then
        reset(f,out_file);
        put(f,"0 => Affichage de l'aide (0 / 1)");-- mémoriser : aide non affcihée
        reset(f,in_file);
      end if;
    elsif aide /= '0' then
      raise EX_PARAM_INVALIDE;
    end if;
    new_line;

  exception
    when EX_PARAM_INVALIDE => raise EX_PARAM_INVALIDE;
  end Accueil;
end p_vuetxt;

package body p_virus is

---------------Primitives d’initialisation d’une partie
  procedure InitPartie(Grille: in out TV_Grille; Pieces: in out TV_Pieces) is
  --{} => {Tous les éléments de Grille ont été initialisés avec la couleur vide
  --      y compris les cases inutilisables
  --      Tous les éléments de Pieces ont été initialisés à false}

  begin --InitPartie
    for l in Grille'range(1) loop
      for c in Grille'range(2) loop
        Grille(l,c) := vide;
      end loop;
    end loop;
    Pieces := (others => false);
  end InitPartie;




  procedure Configurer(f : in out p_piece_io.file_type; nb : in integer; Grille:in out TV_Grille; Pieces: in out TV_Pieces) is
    -- {f ouvert, nb est un numéro de configuration (appelé numéro de partie),
    -- une configuration décrit le placement des pièces du jeu, pour chaque configuration:
    -- * les éléments d’une même pièce(même couleur) sont stockés consécutivement
    -- * il n’y a pas deux pièces mobiles ayant la même couleur
    -- * les deux éléments constituant le virus (couleur rouge)terminent la configuration}
    --    => {Grille a été mis à jour par lecture dans f de la configuration de numéro nb
    --      Pieces a été initialisé en fonction des pièces de cette configuration}
    -- pour test configuration...
    lr : TR_ElemP;
    currconf : natural := 1; --currconf = numéro de la configuration actuelle
  begin
    reset(f,in_file);
    InitPartie(Grille,Pieces);-- réinitialisation de la grille pour enlever pièces résiduelles

    --------------- ON SE DÉPLACE JUSQU'À LA CONFIGURATION NB... ------------------
    if not end_of_file(f) then
      read(f,lr);
      -- Dans le cas où on demande la 1ère config, traiter immédiatement le 1er élement
      if nb<=1 then
        Grille(lr.ligne,lr.colonne) := lr.couleur;
        Pieces(lr.couleur) := true;
      end if;
    end if;

    while currconf<nb loop
      loop
        read(f,lr);
        exit when end_of_file(f) or lr.couleur = rouge; -- Tant qu'on n'atteint pas le rouge (fin de config)
      end loop;
      -- On lit le deuxième rouge si la boucle ne s'est pas arrêtée à cause d'une eof
      read(f,lr);
      currconf:=currconf+1;
    end loop;

    ------------ TÊTE DE LECTURE AU DÉBUT DE LA CONFIGURATION NB -----------------
    read(f,lr);
    while not end_of_file(f) and lr.couleur /= rouge loop -- Tant qu'on n'atteint pas le rouge (fin de config)
      Grille(lr.ligne,lr.colonne) := lr.couleur; -- ..et affecter la case correspondante de la grille à la bonne couleur
      Pieces(lr.couleur) := true;
      read(f,lr);                               -- lire un élément..
    end loop;
      Grille(lr.ligne,lr.colonne) := lr.couleur; -- (premier rouge)
      Pieces(lr.couleur) := true;
      -- On lit le deuxième rouge si la boucle ne s'est pas arrêtée à cause d'une eof
      if not end_of_file(f) then
        read(f,lr);
        Grille(lr.ligne,lr.colonne) := lr.couleur; -- (deuxième rouge)
        Pieces(lr.couleur) := true;
      end if;
  exception
    when text_io.END_ERROR => put_line("Erreur de fin, vérifiez le numéro de configuration.");
  end Configurer;



  procedure PosPiece(Grille: in TV_Grille; coul: in T_coulP) is
    --{} => {la position de la pièce de couleur coul a été affichée si cette pièce est dans Grille
    --      exemple: ROUGE: F4 G5}
    trouve : boolean := false;
  begin
    for l in Grille'range(1) loop--   Pour chaque case..
      for c in Grille'range(2) loop-- de la grille (ligne, colonne)

        if Grille(l,c) = coul then
          --Pour le premier trouvé, afficher le nom de la couleur
          if not trouve then
            put(T_Coul'image(coul) & " :");
            trouve:=true;
          end if;
          -- Affichage des coordonnées
          put(" ");
          put(c);
          put(l,0);
        end if;

      end loop;
    end loop;
    if not trouve then-- Message si pièce non trouvée
      put("Pièce non trouvée.");
    end if;
    new_line;
  end PosPiece;

  function deplacement(prevcoord : in TR_Coord; dir : in T_Direction) return TR_Coord is
    --{} => {résultat : coordonnées correspondant au déplacement de prevcoord
    -- dans la direction dir}
      ldep,cdep : integer := 0;-- valeur du déplacement en ligne et colonne
  begin -- deplacement
    --Décalage vertical (lignes) : -1 pour H, ou +1 pour B
    if image(Dir)(1) = 'H' then
      ldep:=-1;
    else
      ldep:=1;
    end if;
    --Décalage horizontal (colonnes) : -1 pour G, ou +1 pour D
    if image(Dir)(2) = 'G' then
      cdep:=-1;
    else
      cdep:=1;
    end if;
    return (prevcoord.lig + ldep, character'val(character'pos(prevcoord.col) + cdep));
    -- pour la colonne, on passe par val() et pos() pour incrémenter ou décrémenter un caractère

  -- exception
  --   when CONSTRAINT_ERROR => put_line("Impossible de déplacer hors de la grille !"); raise CONSTRAINT_ERROR;
  end deplacement;
-- ---------------CONTRÔLE DU JEU---------------
  function Possible (Grille: in TV_Grille; coul: T_CoulP; Dir : in T_Direction) return boolean is
  --{la pièce de couleur coul est présente dans Grille}
  --  => {résultat= vrai si la pièce de couleur coul peut être déplacée dans la direction Dir}
    ok : boolean := true;
    cposfuture : T_Coul;
    coordfuture : TR_Coord;
  begin
    if coul = blanc then
      return false;
    else
      -- put(ldep);
      -- put(cdep);
      for l in Grille'range(1) loop--   Pour chaque case..
        for c in Grille'range(2) loop-- de la grille (ligne, colonne)
          if Grille(l,c) = coul then
            -- put_line(integer'image(l+ldep));
            -- put(character'val(character'pos(c)+cdep));new_line;
            coordfuture := deplacement((l,c),Dir);
            cposfuture := Grille(coordfuture.lig,coordfuture.col);
            -- put_line("cposfuture : "& T_Coul'image(cposfuture));
            if not (cposfuture=coul or cposfuture=vide) then
              ok := false;
            end if;
          end if;

        end loop;-- Fin double itération..
      end loop;-- sur la grille
      return ok;

    end if;--
  exception
    when CONSTRAINT_ERROR => return false;
  end Possible;

  procedure MajGrille (Grille: in out TV_Grille; coul: in T_coulP; Dir :in T_Direction) is
  --{la pièce de couleur coul peut être déplacée dans la direction Dir}
  --  => {Grille a été mis à jour suite au déplacement}
    NouvGrille : TV_Grille := Grille;
    coordfuture : TR_Coord;
  begin
    -- EFFACER LA PIÈCE DANS LA NOUVELLE GRILLE
    for l in NouvGrille'range(1) loop--   Pour chaque case..
      for c in NouvGrille'range(2) loop-- de la grille (ligne, colonne)
        if NouvGrille(l,c) = coul then
          NouvGrille(l,c) := vide;
        end if;
      end loop;
    end loop;
    -- REMPLIR LA NOUVELLE GRILLE AVEC LES ELEMENTS DÉPLACÉS
    for l in Grille'range(1) loop--   Pour chaque case..
      for c in Grille'range(2) loop-- de la grille (ligne, colonne)
        if Grille(l,c) = coul then
          coordfuture := deplacement((l,c),dir);
          NouvGrille(coordfuture.lig,coordfuture.col) := coul;
        end if;
      end loop;
    end loop;
    -- Remplacer grille par nouvelle grille
    Grille := NouvGrille;
  end MajGrille;

  function Guerison(Grille: in TV_Grille) return boolean is
  --{} => {résultat = le virus (pièce rouge) est prêt à sortir (position coin haut gauche)}
  begin
    return (Grille(1,'A')=rouge and Grille(2,'B')=rouge);
  end Guerison;

  function invDir(dir : in T_Direction) return T_Direction is
  --{} => {resultat : direction opposée à dir}
  begin -- invDir
    case dir is
      when hg => return bd;
      when hd => return bg;
      when bg => return hd;
      when bd => return hg;
    end case;
  end invDir;
  
  function PieceMobile(grille : in TV_Grille; coul : in T_CoulP) return boolean is
  --{} => {résultat = true si la pièce de couleur coul peut bouger dans uen direction au moins}
    mobile : boolean := false;
    i : natural := 0;
  begin -- PieceMobile
    while i < 4 and then not Possible(grille,coul,T_Direction'val(i)) loop
      i:=i+1;
    end loop;
  return i < 4 or Possible(grille,coul,T_Direction'val(3));
  end PieceMobile;

end p_virus;

with p_virus,p_fenbase,Forms,text_io,p_score,p_esiut; use p_virus,p_fenbase,Forms,text_io,p_score,p_esiut;
use p_score.p_score_io;

package p_vue_graph is



  --==============================================================================
  --========================= CREATIONS FENETRES =================================
  --==============================================================================

  procedure CreeFenMenu(F : in out TR_Fenetre; tailleFenH,tailleFenV : in natural);
  --{InitialiserFenetres lancé} => {La fenêtre de menu a été créée}

  procedure CreeFenGrille(F : in out TR_Fenetre; tailleFenH,tailleFenV,tailleEcartBt : in natural);
  --{InitialiserFenetres lancé} => {La fenêtre de grille a été créée}

  procedure CreeFenAide (F : in out TR_Fenetre; tailleFenH,tailleFenV : in natural);
  --{InitialiserFenetres lancé} => {La fenêtre d'aide a été créée}

  procedure CreeFenVic(F : in out TR_Fenetre; tailleFenH,tailleFenV : in natural);
  --{InitialiserFenetres lancé} => {La fenêtre de victoire a été créée}

  procedure CreeFenPopup(F : in out TR_Fenetre; tailleFenH,tailleFenV : in natural);
  --{InitialiserFenetres lancé} => {La fenêtre de PopUp a été créée}

  --==============================================================================
  --============================ MAJ ELEMENTS ====================================
  --==============================================================================

  procedure MAJFenGrille(F : in out TR_Fenetre; grille : in TV_Grille);
  --{fenêtre grille créée} => {Affichage de la grille mis à jour avec les couleurs du tableau Grille}

  procedure MajBtDir(F : in out TR_Fenetre; grille : in TV_Grille; coul : in T_CoulP);
  --{} => {les boutons de directions affichés correspondent uniquement aux directions possibles
  -- pour la couleur coul}

  --==============================================================================
  --================== GESTION FONCTIONNEMENT FENETRES ===========================
  --==============================================================================

  procedure LancerPartie(
      FMain,FVic,FPopup : in out TR_Fenetre;
      Grille : in out TV_Grille;
      Pieces : in out TV_Pieces;
      numLvl : in natural;
      f : in out p_piece_io.file_type;
      pseudo : in String);
  --{La fenêtre de jeu a été créée et configurée, un niveau a été chargé}
  -- => {la partie s'est déroulée jusqu'à la victoire ou l'annulation}

  procedure LancerAide (FMenu,FAide : in out TR_Fenetre);
  --{La fenêtre d'aide a été créée et configurée} => {La fenêtre d'Aide a été affichée}

  procedure LancerVic(F : in out TR_Fenetre; replay : out boolean; numLvl : in natural; cptCoup : in positive; temps : in float; pseudo : in String);
  --{La fenêtre de victoire a été créée et configurée} =>
  --{Les résultats de partie sont interprétés et la fenêtre de victoire est affichée}
  procedure LancerPopup(F : in out TR_Fenetre; result : out boolean);
  --{La fenêtre PopUp de confirmation a été créée et configurée} =>
  --{Le fenêtre PopUp de conirmation met en pause la partie et quitte ou non la partie selon choix}

  --==============================================================================
  --=============================== DIVERS =======================================
  --==============================================================================
  function ConvertCoul(coul : in T_Coul) return FL_PD_COL;
  --{} => {résultat : couleur correspondante en FL_PD_COL}
  function ConvertTemps(t : in float) return string;
    --{} => {résultat : chaîne correspondante avec 3 décimales ; ex : X.XXX}

end p_vue_graph;

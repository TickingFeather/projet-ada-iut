with p_virus,p_historique,p_fenbase,Forms; use p_virus,p_historique,p_fenbase,Forms;

package body p_vue_graph is
  --==============================================================================
  --========================= CREATIONS FENETRES =================================
  --==============================================================================
  procedure CreeFenGrille(F : in out TR_Fenetre; tailleFenH,tailleFenV,tailleEcartBt : in natural) is
    --{InitialiserFenetres lancé} => {La fenêtre de grille a été créée}
    offsetGx : natural := 50;
    offsetGy : natural := 50;
    nomBt : string(1..5);
    strCol : String(1..2) := ("  ");
  begin -- CreeFenGrille
    F := DebutFenetre("AntiVirus v1.3",tailleFenH,tailleFenV);

    --Affichage image de sortie
    AjouterImage(F,"imageSortie","ressources/sortie.xpm","",5,5,100,50);

    ChangerCouleurFond(F,"fond",FL_RIGHT_BCOL);

    --Affichage du titre en jeu
    AjouterTexte(F,"titreJeu","Anti-Virus",(tailleFenH/2)-(200/2),7,200,50);
    ChangerAlignementTexte(F,"titreJeu",FL_ALIGN_CENTER);
    ChangerTailleTexte(F,"titreJeu",25);
    ChangerStyleTexte(F,"titreJeu",FL_BOLDITALIC_STYLE);
    ChangerCouleurTexte(F,"titreJeu",FL_WHITE);
    ChangerCouleurFond(F,"titreJeu",FL_RIGHT_BCOL);

    --Affichage du numéro du niveau
    AjouterTexte(F,"numLvl","Niveau :",tailleFenH-270,30,100,50);
    ChangerAlignementTexte(F,"numLvl",FL_ALIGN_LEFT);
    ChangerTailleTexte(F,"numLvl",15);
    ChangerStyleTexte(F,"numLvl",FL_ITALIC_STYLE);
    ChangerCouleurTexte(F,"numLvl",FL_WHITE);
    ChangerCouleurFond(F,"numLvl",FL_RIGHT_BCOL);

    --Affichage compteur de coups
    AjouterTexte(F,"cptCoup","Coups : 0",tailleFenH-270,70,100,50);
    ChangerAlignementTexte(F,"cptCoup",FL_ALIGN_LEFT);
    ChangerTailleTexte(F,"cptCoup",15);
    ChangerStyleTexte(F,"cptCoup",FL_ITALIC_STYLE);
    ChangerCouleurTexte(F,"cptCoup",FL_WHITE);
    ChangerCouleurFond(F,"cptCoup",FL_RIGHT_BCOL);

    --Affichage chrono
    AjouterChrono(F,"chrono","",tailleFenH-270,160,100,50);
    ChangerTailleTexte(F,"chrono",15);
    ChangerStyleTexte(F,"chrono",FL_BOLD_STYLE);
    ChangerCouleurTexte(F,"chrono",FL_WHITE);
    ChangerCouleurFond(F,"chrono",FL_RIGHT_BCOL);
    --et sa légende
    AjouterTexte(F,"legChrono","Temps : ",tailleFenH-270,110,100,50);
    ChangerAlignementTexte(F,"legChrono",FL_ALIGN_LEFT);
    ChangerTailleTexte(F,"legChrono",15);
    ChangerStyleTexte(F,"legChrono",FL_ITALIC_STYLE);
    ChangerCouleurTexte(F,"legChrono",FL_WHITE);
    ChangerCouleurFond(F,"legChrono",FL_RIGHT_BCOL);

    --Indicateur de couleur sélectionnée
    AjouterBouton(F,"indCouleur","",tailleFenH-350,offsetGy+100,25,tailleFenV-offsetGy-200);
    ChangerEtatbouton(F,"indCouleur",Arret);

    -- AjouterBouton(F,"BoutonStart","START",0,0,150,200);
    for i in 1..7 loop
      for j in 1..7 loop

        --Création des boutons de la grille de jeu
        if i mod 2 = j mod 2 then
          nomBt := "c"&integer'image(i)&integer'image(j);
          AjouterBoutonRond(F,nomBt,"",offsetGx+(i-1)*tailleEcartBt,offsetGy+25+(j-1)*tailleEcartBt,100);

        end if;
      end loop;
    end loop;

    -- Flèches directionnelles
    AjouterBoutonImage(F,"d HG","","ressources/fleche-hg.xpm",tailleFenH-200-25,tailleFenV-150-75,75,75);
    AjouterBoutonImage(F,"d HD","","ressources/fleche-hd.xpm",tailleFenH-100-25,tailleFenV-150-75,75,75);
    AjouterBoutonImage(F,"d BD","","ressources/fleche-bd.xpm",tailleFenH-100-25,tailleFenV-50-75,75,75);
    AjouterBoutonImage(F,"d BG","","ressources/fleche-bg.xpm",tailleFenH-200-25,tailleFenV-50-75,75,75);

    --Bouton Annuler
    AjouterBouton(F,"annul","Annuler",tailleFenH-100-25,tailleFenV-150-75-25-50,75,50);

    --Bouton quitter
    AjouterBouton(F,"btQuitJeu","Quitter",tailleFenH-25-100,25,100,25);

    --Bouton reset
    AjouterBouton(F,"btReset","Recommencer",tailleFenH-25-100,60,100,25);

    FinFenetre(F);
  end CreeFenGrille;

  procedure CreeFenMenu(F : in out TR_Fenetre; tailleFenH,tailleFenV : in natural) is
    --{InitialiserFenetres lancé} => {La fenêtre de menu a été créée}
    offsetx,offsetY,ecartBtx,ecartBty : natural;
    -- nomBt : string(1..5);
    tailleBtx : integer := 85;
    tailleBty : integer := 25;

  begin -- CreeFenMenu
    ecartBtx := 15;
    ecartBty := 10;
    offsetx := 25;
    offsety := tailleFenV-(tailleBty+ecartBty)*5 - 60;

    F := DebutFenetre("AntiVirus - Menu",tailleFenH,tailleFenV);

    AjouterImage(F,"imgTitre","ressources/av-titre.xpm","",0,0,520,209);

    --Bouton Aide
    AjouterBouton(F,"btAide","Aide",(tailleFenH/2)-(150/2),220,150,25);

    --Champ de saisie pseudo
    AjouterChamp(F,"champPseudo","Saisir pseudo","",(tailleFenH/2)-(70/2),(tailleFenV/2)-10,150,30);

    ChangerCouleurFond(F,"fond",FL_DARKGOLD);

    -- Boucle de génération de la grille des niveaux
    for numlevel in 1..20 loop
      AjouterBouton(F,"lv"&integer'image(numlevel),"Niveau"&integer'image(numlevel) , offsetx+ (tailleBtx + ecartBtx)*((numlevel-1) mod 4) , offsety+ (tailleBty+ecartBty)*((numlevel-1) / 4) , tailleBtx,tailleBty);
      -- Coords : |En x : offset + (tailleBtx + espace entre boutons)*((numlevel-1) mod nombre de boutons par ligne) | En y : offset + (tailleBty+espace entre boutons)*((numlevel-1) / nombre de boutons par ligne)

      --Afficher le texte de catégories de difficulté
      case numlevel is
        when 4 => AjouterTexte(F,"txtDiff" & integer'image(numlevel/4),"Facile",offsetx+ (tailleBtx + ecartBtx)*((numlevel-1) mod 4) + tailleBtx +10,offsety+ (tailleBty+ecartBty)*((numlevel-1) / 4) -2,70,25);
        when 8 => AjouterTexte(F,"txtDiff" & integer'image(numlevel/4),"Moyen",offsetx+ (tailleBtx + ecartBtx)*((numlevel-1) mod 4) + tailleBtx+10,offsety+ (tailleBty+ecartBty)*((numlevel-1) / 4) -2,70,25);
        when 12 => AjouterTexte(F,"txtDiff" & integer'image(numlevel/4),"Difficile",offsetx+ (tailleBtx + ecartBtx)*((numlevel-1) mod 4) + tailleBtx+10,offsety+ (tailleBty+ecartBty)*((numlevel-1) / 4) -2,70,25);
        when 16 => AjouterTexte(F,"txtDiff" & integer'image(numlevel/4),"Expert",offsetx+ (tailleBtx + ecartBtx)*((numlevel-1) mod 4) + tailleBtx+10,offsety+ (tailleBty+ecartBty)*((numlevel-1) / 4) -2,70,25);
        when 20 => AjouterTexte(F,"txtDiff" & integer'image(numlevel/4),"Mythique",offsetx+ (tailleBtx + ecartBtx)*((numlevel-1) mod 4) + tailleBtx+10,offsety+ (tailleBty+ecartBty)*((numlevel-1) / 4) -2,70,25);
        when others => null;
      end case;

      --Appliquer le style aux catégories de difficulté
      ChangerCouleurFond(F,"txtDiff" & integer'image(numlevel/4),FL_DARKGOLD);
      ChangerStyleTexte(F,"txtDiff" & integer'image(numlevel/4),FL_BOLD_STYLE);

      --Gestion des couleurs des différentes gcatégories de difficulté
      if numlevel >=1 and numlevel<=4 then
        ChangerCouleurFond(F,"lv"&integer'image(numlevel),FL_CHARTREUSE);
      elsif numlevel >=5 and numlevel<=8 then
        ChangerCouleurFond(F,"lv"&integer'image(numlevel),FL_DODGERBLUE);
      elsif numlevel >=9 and numlevel<=12 then
        ChangerCouleurFond(F,"lv"&integer'image(numlevel),FL_YELLOW);
      elsif numlevel >=13 and numlevel<=16 then
        ChangerCouleurFond(F,"lv"&integer'image(numlevel),FL_DARKORANGE);
      elsif numlevel >=17 and numlevel<=20 then
        ChangerCouleurFond(F,"lv"&integer'image(numlevel),FL_RED);
      end if;

    end loop;
    -- Bouton Quitter
    AjouterBouton(F,"btQuitMenu","Quitter",(tailleFenH/2)-(150/2),tailleFenV-(25+25),150,25);

    FinFenetre(F);
  end CreeFenMenu;

  procedure CreeFenAide (F : in out TR_Fenetre; tailleFenH,tailleFenV : in natural) is
    --{InitialiserFenetres lancé} => {La fenêtre d'aide a été créée}

  begin -- CreeFenAide
    F := DebutFenetre("AntiVirus - Aide",tailleFenH,tailleFenV);

    ChangerCouleurFond(F,"fond",FL_RIGHT_BCOL);
    -- Affichage titre Aide
    AjouterTexte(F,"titreAide","AntiVirus",(tailleFenH/2)-(200/2),35,200,35);
    ChangerAlignementTexte(F,"titreAide",FL_ALIGN_CENTER);
    ChangerTailleTexte(F,"titreAide",25);
    ChangerStyleTexte(F,"titreAide",FL_BOLDITALIC_STYLE);
    ChangerCouleurTexte(F,"titreAide",FL_WHITE);
    ChangerCouleurFond(F,"titreAide",FL_RIGHT_BCOL);

    --==================================TEXTE FENÊTRE AIDE==================================
    AjouterTexte(F,"§ 1l 1Aide","Votre objectif est de deplacer la piece rouge dans le coin",(tailleFenH/2)-((tailleFenH-20)/2),80,tailleFenH-20,25);
    AjouterTexte(F,"§ 1l 2Aide","superieur gauche. Vous pouvez deplacer les pieces en",(tailleFenH/2)-((tailleFenH-20)/2),80+20,tailleFenH-20,25);
    AjouterTexte(F,"§ 1l 3Aide","cliquant dessus puis en selectionnant une direction.",(tailleFenH/2)-((tailleFenH-20)/2),80+20+20,tailleFenH-20,25);
    AjouterTexte(F,"§ 1l 4Aide","La barre verticale indique la couleur selectionnee.",(tailleFenH/2)-((tailleFenH-20)/2),80+20+20+20,tailleFenH-20,25);

    AjouterTexte(F,"§ 2l 1Aide","A tout moment, vous pouvez recommencer le niveau,",(tailleFenH/2)-((tailleFenH-20)/2),(80+20+20+20)+50,tailleFenH-20,25);
    AjouterTexte(F,"§ 2l 2Aide","annuler votre dernier coup ou quitter la partie. ",(tailleFenH/2)-((tailleFenH-20)/2),(80+20+20+20)+50+20,tailleFenH-20,25);

    AjouterTexte(F,"§ 3l 1Aide","Les pieces blanches sont fixes et par consequent",(tailleFenH/2)-((tailleFenH-20)/2),((80+20+20+20)+50+20)+50,tailleFenH-20,25);
    AjouterTexte(F,"§ 3l 2Aide","ne peuvent pas etre selectionnees.",(tailleFenH/2)-((tailleFenH-20)/2),((80+20+20+20)+50+20)+50+20,tailleFenH-20,25);

    --On applique le style à tous les paragraphes de l'aide
    for i in 1..3 loop
      for j in 1..4 loop
        ChangerCouleurTexte(F,"§" & integer'image(i) &"l"&integer'image(j)&"Aide",FL_WHITE);
        ChangerCouleurFond(F,"§" & integer'image(i) &"l"&integer'image(j)&"Aide",FL_RIGHT_BCOL);
      end loop;
    end loop;

    --Bouton retour au menu
    AjouterBouton(F,"btQuitAide","Retour au menu",(tailleFenH/2)-(250/2),((tailleFenV-80)+(25/2)),250,25);


    FinFenetre(F);
  end CreeFenAide;

  procedure CreeFenVic(F : in out TR_Fenetre; tailleFenH,tailleFenV : in natural) is
    --{} => {La fenêtre de victoire a été créée}
  begin
    F := DebutFenetre("Victoire !",tailleFenH,tailleFenV);

    --Image d'illustration
    AjouterImage(F,"imgVic","ressources/victoire.xpm","",0,0,400,222);

    --Fond orange
    ChangerCouleurFond(F,"fond",FL_DARKGOLD);

    --Titre
    AjouterTexte(F,"titreVic","Victoire !",(tailleFenH/2)-(200/2),235,200,40);
    ChangerAlignementTexte(F,"titreVic",FL_ALIGN_CENTER);
    ChangerTailleTexte(F,"titreVic",25);
    ChangerStyleTexte(F,"titreVic",FL_BOLDITALIC_STYLE);
    ChangerCouleurFond(F,"titreVic",FL_DARKGOLD);

    --Texte de résumé : niveau, nombre de coups
    AjouterTexte(F,"txtVic"," ",(tailleFenH/2)-((tailleFenH-20)/2),300,tailleFenH-20,25);
    ChangerTailleTexte(F,"txtVic",10);
    ChangerStyleTexte(F,"txtVic",FL_BOLD_STYLE);
    ChangerAlignementTexte(F,"txtVic",FL_ALIGN_CENTER);
    ChangerCouleurFond(F,"txtVic",FL_DARKGOLD);

    --Texte annonceur du temps
    AjouterTexte(F,"txtPreTemps","Votre temps : ",28,340,100,40);
    ChangerCouleurFond(F,"txtPreTemps",FL_DARKGOLD);

    --Affichage du temps réalisé
    AjouterTexte(F,"txtTemps","",145,340,100,40);
    ChangerTailleTexte(F,"txtTemps",20);
    ChangerStyleTexte(F,"txtTemps",FL_BOLD_STYLE);
    ChangerAlignementTexte(F,"txtTemps",FL_ALIGN_CENTER);
    ChangerCouleurFond(F,"txtTemps",FL_DARKGOLD);

    --Titre du scoreboard
    AjouterTexte(F,"titleScoreBoard","Meilleurs scores",(tailleFenH/2)-((tailleFenH-100)/2),390,tailleFenH-100,25);
    ChangerAlignementTexte(F,"titleScoreBoard",FL_ALIGN_CENTER);
    ChangerCouleurFond(F,"titleScoreBoard",FL_DARKGOLD);

    ---------Affichage du scoreboard------------------
    AjouterTexte(F,"ScoreBoardS 1L 1","",(tailleFenH/2)-4*((tailleFenH/3 -50)/2),430,tailleFenH/3 -50,25);
    AjouterTexte(F,"ScoreBoardS 1L 2","",(tailleFenH/2)-4*((tailleFenH/3 -50)/2),430+25,tailleFenH/3 -50,25);
    AjouterTexte(F,"ScoreBoardS 1L 3","",(tailleFenH/2)-4*((tailleFenH/3 -50)/2),430+25+25,tailleFenH/3 -50,25);

    AjouterTexte(F,"ScoreBoardS 2L 1","",(tailleFenH/2)-((tailleFenH/3 -50)/2),430,tailleFenH/3 -50,25);
    AjouterTexte(F,"ScoreBoardS 2L 2","",(tailleFenH/2)-((tailleFenH/3 -50)/2),430+25,tailleFenH/3 -50,25);
    AjouterTexte(F,"ScoreBoardS 2L 3","",(tailleFenH/2)-((tailleFenH/3 -50)/2),430+25+25,tailleFenH/3 -50,25);

    AjouterTexte(F,"ScoreBoardS 3L 1","",(tailleFenH/2)+2*((tailleFenH/3 -50)/2),430,tailleFenH/3 -50,25);
    AjouterTexte(F,"ScoreBoardS 3L 2","",(tailleFenH/2)+2*((tailleFenH/3 -50)/2),430+25,tailleFenH/3 -50,25);
    AjouterTexte(F,"ScoreBoardS 3L 3","",(tailleFenH/2)+2*((tailleFenH/3 -50)/2),430+25+25,tailleFenH/3 -50,25);

    --Applique le style à tous les champs texte du scoreboard
    for s in 1..3 loop
      for l in 1..3 loop
        ChangerCouleurFond(F,"ScoreBoardS" & integer'image(s)&"L" & integer'image(l),FL_DARKGOLD);
        ChangerAlignementTexte(F,"ScoreBoardS" & integer'image(s)&"L" & integer'image(l),FL_ALIGN_CENTER);
      end loop;
    end loop;

    --BOUTONS
    --Bouton Rejouer
    AjouterBouton(F,"btReplayVic","Rejouer",(tailleFenH/2)-(125)-10,((tailleFenV-80)+(25/2)),125,25);
    --Bouton retour au menu
    AjouterBouton(F,"btQuitVic","Retour au menu",(tailleFenH/2)+10,((tailleFenV-80)+(25/2)),125,25);
    FinFenetre(F);
  end CreeFenVic;

  procedure CreeFenPopup(F : in out TR_Fenetre; tailleFenH,tailleFenV : in natural) is
    --{} => {}
    tailleBtx : natural := 150;
    tailleBty : natural := 50;
  begin -- CreeFenPopup
    F := DebutFenetre("Quitter ?",tailleFenH,tailleFenV);
    --Message de confirmation
    AjouterTexte(F,"msgPopup","Voulez-vous vraiment quitter la partie ?",(tailleFenH/2)-((tailleFenH-25)/2),25,tailleFenH-25,20);
    ChangerAlignementTexte(F,"msgPopup",FL_ALIGN_CENTER);

    --Boutons de choix
    AjouterBouton(F,"btOui","Oui",(tailleFenH/2)-tailleBtx-10, tailleFenV-tailleBty-20, tailleBtx, tailleBty);
    AjouterBouton(F,"btNon","Non",(tailleFenH/2)+10, tailleFenV-tailleBty-20, tailleBtx, tailleBty);

    FinFenetre(F);
  end CreeFenPopup;
  --==============================================================================
  --============================ MAJ ELEMENTS ====================================
  --==============================================================================

  procedure MAJFenGrille(F : in out TR_Fenetre; grille : in TV_Grille) is
    --{fenêtre grille créée} => {Affichage de la grille mis à jour avec les couleurs du tableau Grille}
    nomBt : string(1..5);
    coulBt : p_fenbase.T_Couleur;
    coulActuelle : T_Coul;
  begin -- MAJFenGrille

    for x in 1..7 loop
      for y in 1..7 loop
        nomBt := "c"&integer'image(x) & integer'image(y);
        coulActuelle := Grille(y, T_Col'val(T_Col'pos('A') + x - 1) );

        if x mod 2 = y mod 2 then-- seulement pour les cases à parité égale (cases actives)
          -- put(integer'image(y));
          -- put(T_Col'val(T_Col'pos('A') + x - 1));
          -- put(T_Coul'image(Grille(y, T_Col'val(T_Col'pos('A') + x - 1) )));
          coulBt := ConvertCoul(coulActuelle);

          if not (coulActuelle=blanc or coulActuelle=vide) then
            ChangerEtatbouton(F,nomBt,Marche);
          else
            ChangerEtatbouton(F,nomBt,Arret);
          end if;

          ChangerCouleurFond(F,nomBt,coulBt);
        end if;

      end loop;
    end loop;

  end MAJFenGrille;


  --==============================================================================================
  --==============================================================================================


--==============================================================================================
--==============================================================================================

  procedure MajBtDir(F : in out TR_Fenetre; grille : in TV_Grille; coul : in T_CoulP) is
    --{} => {les boutons de directions affichés correspondent uniquement aux directions possibles
    -- pour la couleur coul}

  begin -- MajBtDir
    for dir in T_Direction loop
      if Possible(grille,coul,dir) then
        MontrerElem(F, "d "&T_Direction'image(dir));
        -- put_line("bt visible : d "&T_Direction'image(dir));
      else
        CacherElem(F, "d "&T_Direction'image(dir));
        -- put_line("bt caché : d "&T_Direction'image(dir));
      end if;
    end loop;
  end MajBtDir;



  --==============================================================================
  --================== GESTION FONCTIONNEMENT FENETRES ===========================
  --==============================================================================



  procedure LancerPartie(
      FMain,FVic,FPopup : in out TR_Fenetre;
      Grille : in out TV_Grille;
      Pieces : in out TV_Pieces;
      numLvl : in natural;
      f : in out p_piece_io.file_type;
      pseudo : in String) is
    --{La fenêtre de jeu a été créée, configurée et affichée, un niveau a été chargé}
    -- => {la partie s'est déroulée jusqu'à la victoire ou l'annulation}

    cptcoup : positive;-- compteur de coups/de tours
    temps : float := 0.0;
    --Variables de sélection du déplacement (couleur et direction)
    coulSelect : T_CoulP;
    dirSelect : T_Direction;
    --Variables pour l'historique
    his : TV_Hist(1..100);
    itop : natural;
    dep : TR_Dep;

    --Booléens de contrôle des cas
    -- (nécessaires puisque les conditions sur btClique sont inutilisables
    -- en dehors de la déclaration dynamique, elle-même contrainte dans la boucle
    -- des tours)
    rejouer : boolean := true;-- Si true, relance une nouvelle partie (de zéro)
    terminer : boolean;-- Si true, termine les tours de jeu en ignorant
    --                             la condition de victoire

    confirmQuit : boolean := false;-- Paramètre nécessaire pour popup de confirmation
  begin

    --BOUCLE CONTENANT UNE PARTIE COMPLÈTE : MISE EN PLACE ET TOURS DE JEU --
    while rejouer loop
      -- put_line("Début partie"); --debug
      ------------------------------ INITIALISATIONS -----------------------------
      cptcoup := 1;-- compteur de coups à 1
      -- Historique vide
      his := (others => (blanc,hg));
      itop := his'first-1;
      --Booléens de contrôle à false
      terminer := false;
      rejouer := false;
      ----------------------------------------------------------------------------

      ----------------------------- MISE EN PLACE --------------------------------

      -- Mettre à jour indicateur de niveau
      ChangerTexte(Fmain,"numLvl","Niveau :" & integer'image(numLvl));
      Configurer(f,numLvl,Grille,Pieces);-- Configurer la grille pour le niveau donné
      MAJFenGrille(FMain,Grille);--       ..et la mettre à jour

      MajBtDir(FMain,grille,blanc);-- Appel de MajBtDir pour la couleur blanc (non mobile)..
      --    .. pour faire disparaître tous les boutons tant qu'aucune couleur n'est cliquée.
      CacherElem(FMain,"annul");-- Cacher le bouton annuler par défaut,
      --Définir l'indicateur de couleur en gris par défaut
      ChangerCouleurFond(FMain,"indCouleur",ConvertCoul(vide));
      --Mettre à jour l'affichage du compteur de coups
      ChangerTexte(Fmain,"cptCoup","Coups : " & integer'image(cptcoup-1));

      -- (re)démarrer le timer
      ChangerTempsMinuteur(FMain,"chrono",3600.0);
      RepriseTimer(FMain,"chrono");-- au cas où il soit en pause

      ---------------------- BOUCLE PRINCIPALE DES TOURS DE JEU --------------------
      ------------------- Si on en sort, la partie est finie (gagnée ou annuler)

      while not ( Guerison(grille) or terminer ) loop

        -- put_line(float'image(3600.0 - ConsulterTimer(FMain,"chrono")));-- Debug : temps écoulé
        declare
          -- Attendre un bouton et ranger son nom dans btClique, déclaré dynamiquement
          btClique : string := AttendreBouton(FMain);
        begin
          if btClique(1) = 'c' then-- si le bouton cliqué est une CASE de la grille,
            -- ..parser ses coordonnées (grâce au nom) et récupérer la couleur dans la grille
            coulSelect := grille(integer'value(btClique(4..5)) , T_Col'val(integer'value(btClique(2..3)) + T_Col'pos('A') - 1));
            -- put_line("Couleur sélectionnée : " & T_coulP'image(coulSelect));-- debug : couleur sélectionnée
            ChangerCouleurFond(FMain,"indCouleur",ConvertCoul(coulSelect));

          elsif btClique(1) = 'd' then-- sinon si le bouton cliqué est une DIRECTION
            -- ..parser sa direction (grâce au nom)
            dirSelect := T_Direction'value(btClique(3..4));
            MajGrille(grille,coulSelect,dirSelect);-- Appliquer le déplacement à la grille
            MAJFenGrille(FMain,grille);-- et mettre à jour la fenêtre de la grille

            cptcoup:=cptcoup+1;-- Chaque déplacement vaut un coup, ne pas oublier de le compter
            -- et de le stocker dans l'historique
            dep := (coulSelect,dirSelect);
            empilerCoup(his,itop,dep);

          elsif btClique="annul" then-- sinon si le bouton cliqué est Annuler
            -- ANNULATION COUP PRÉCÉDENT --
            depilerCoup(his,itop,dep);-- On retire un déplacement de l'historique,
            -- on l'applique en inversant la direction,
            MajGrille(grille,dep.couleur,invDir(dep.dir));
            MAJFenGrille(FMain,grille);
            cptcoup := cptcoup-1;-- et on décrémente le compteur de coups

          elsif btClique = "btReset" then
            -- Utilisation des booléens de contrôle nécessaire
            terminer := true;-- Terminer les tours de jeu
            rejouer := true;-- mais relancer la partie

          elsif btClique="btQuitJeu" then
            -- Mettre en pause le chrono pendant le popup
            PauseTimer(FMain,"chrono");
            --Utiliser le popup pour définir Terminer
            LancerPopup(FPopup,terminer);
            RepriseTimer(FMain,"chrono");
            rejouer := false;--mais forcer Rejouer à false
          end if;
          --  Après chaque clic (couleur sélectionnée OU déplacement),
          --  mettre à jour les boutons directionnels.
          MajBtDir(FMain,grille,coulSelect);
          --Mettre à jour l'affichage du compteur de coups
          ChangerTexte(Fmain,"cptCoup","Coups : " & integer'image(cptcoup-1));
          -- Et mettre à jour le bouton Annuler selon si l'historique est vide
          if histoVide(his,itop) then
            CacherElem(FMain,"annul");
          else
            MontrerElem(FMain,"annul");
          end if;

        end;-- Fin déclaration dynamique
        -- put_line("terminer : "&boolean'image(terminer));
        -- put_line("rejouer : "&boolean'image(rejouer));
      end loop;
      ---------------------- FIN BOUCLE PRINCIPALE DES TOURS DE JEU --------------
      -- Si on arrive ici, c'est qu'il y a victoire OU qu'on a quitté manuellement
      PauseTimer(FMain,"chrono");-- Arrêter le chrono

      if Guerison(grille) then-- Victoire !
        --Récupérer temps, càd temps maximum moins temps chronométré
        temps := 3600.0 - ConsulterTimer(FMain,"chrono");
        LancerVic(FVic,rejouer,numLvl,cptcoup-1,temps,pseudo);-- Lancer la fenêtre de victoire
        -- Si elle passe Rejouer à true, on reboucle pour une nouvelle partie ;
        -- sinon, la fonction LancerPartie se termine et on revient au menu
      end if;

    end loop;

  exception
    when EX_FIN_PARTIE =>
      CacherFenetre(FMain);
      -- put_line("Partie annulée");-- Debug
  end LancerPartie;

--==============================================================================================
--==============================================================================================


  procedure LancerAide (FMenu,FAide : in out TR_Fenetre) is
    --{La fenêtre d'aide a été créée et configurée} => {}

  begin -- LancerAide
    MontrerFenetre(FAide);--Afficher la fenêtre de jeu
    CacherFenetre(FMenu);-- et cacher la fenêtre de menu

    declare
      btPresseAide : string := AttendreBouton(FAide);
    begin
      if btPresseAide = "btQuitAide" then
        MontrerFenetre(FMenu);--Afficher la fenêtre de jeu
        CacherFenetre(FAide);-- et cacher la fenêtre de menu
      end if;
    end;
  end LancerAide;

  procedure LancerVic(F : in out TR_Fenetre; replay : out boolean; numLvl : in natural; cptCoup : in positive; temps : in float; pseudo : in String) is
    --{} => {}
    fscore : p_score_io.file_type;
    vNbElem : natural;
  begin -- LancerVic
    --On ouvre le fichier des score ou on le crée s'il n'existe pas encore
    begin
      Open(fscore,in_file,"scores.bin");
    exception
      when p_score_io.NAME_ERROR => Create(fscore,in_file,"scores.bin");
    end;
    --On ajoute le score au fichier
    AjouterScore(fscore,numLvl,pseudo,cptCoup,temps);
    vNbElem := nbElem(fscore); --Compte le nomvre d'éléments du fichier (nécessaire pour getScroreConfig)
    reset(fscore,in_file); --On reset le fichier pour getScroreConfig
    declare
      --On rRécupère dans V les scores pour ce niveau du meilleur au moins bon
      V : TV_Score := getScoreConfig(fscore,numLvl,vNbElem);
      i : positive := V'first;
    begin
      --Pour les trois premiers scores ou moins
      while i < V'last+1 and then i <= 4 loop
        --On change le texte dans le scoreboard
        ChangerTexte(F,"ScoreBoardS" & integer'image(i) & "L 1",V(i).Pseudo);
        ChangerTexte(F,"ScoreBoardS" & integer'image(i) & "L 2",ConvertTemps(V(i).temps) & " s");
        ChangerTexte(F,"ScoreBoardS" & integer'image(i) & "L 3",integer'image(V(i).nbCoup) & "Coups");
        i := i+1;
      end loop;

      MontrerFenetre(F);--Afficher la fenêtre de jeu
      ChangerTexte(F,"txtVic","Vous avez triomphe du niveau" & integer'image(numLvl) & " en" & integer'image(cptCoup) & " coups !");
      ChangerTexte(F,"txtTemps",ConvertTemps(temps));
      declare
        btClique : string := AttendreBouton(F);
      begin
        replay := (btClique = "btReplayVic");
      end;
      close(fscore);
    end;


    CacherFenetre(F);-- et cacher la fenêtre de menu
  end LancerVic;

  procedure LancerPopup(F : in out TR_Fenetre; result : out boolean) is
    --{} => {}
  begin -- LancerPopup
    MontrerFenetre(F);
    declare
      btClique : string := AttendreBouton(F);
    begin
      result := (btClique = "btOui");
    end;
    CacherFenetre(F);
  end LancerPopup;


  --==============================================================================
  --=============================== DIVERS =======================================
  --==============================================================================


  function ConvertCoul(coul : in T_Coul) return FL_PD_COL is
    --{} => {résultat : couleur correspondante en FL_PD_COL}
  begin -- ConvertCoul
    case coul is
      when vide => return FL_BOTTOM_BCOL;
      when rouge => return FL_RED;
      when turquoise => return FL_DARKCYAN;
      when orange => return FL_DARKORANGE;
      when rose => return FL_ORCHID;
      when marron => return FL_DARKTOMATO;
      when bleu => return FL_DODGERBLUE;
      when violet => return FL_SLATEBLUE;
      when vert => return FL_CHARTREUSE;
      when jaune => return FL_YELLOW;
      when blanc => return FL_WHITE;
    end case;
  end ConvertCoul;

  function ConvertTemps(t : in float) return string is
    --{} => {résultat : chaîne correspondante avec 3 décimales ; ex : X.XXX}
  begin
    return integer'image(integer(t * 100.0) / 100) & "." & (if (integer(t * 100.0) mod 100 < 10) then (integer'image(integer(t * 100.0) mod 100)(2..2)) else (integer'image(integer(t * 100.0) mod 100)(2..3)));
  end ConvertTemps;

end p_vue_graph;

package body p_historique is
  function histoVide(h : in TV_Hist; itop : in natural) return boolean is
    --{} => {résultat : true si historique vide}
  begin -- histoVide
    return itop<h'first;
  end histoVide;

  procedure empilerCoup(h : in out TV_Hist; itop : in out natural; val : in TR_Dep) is
  --{} => {val a été ajoutée à l'historique, qui a été reculé
  -- d'un cran (1re valeur oubliée) SI il était plein,et itop est l'indice du sommet de la pile}
  begin
    if itop /= h'last then-- si historique non plein,
      itop:=itop+1;--        incrémenter itop
    else-- sinon,
      --   "reculer" tout le vecteur de 1 cran (on perd le 1er élément)
      h(h'first..h'last-1) := h(h'first+1..h'last);
      -- itop correspond donc bien au premier emplacement libre (ici h'last)
    end if;
    h(itop) := val;-- et ranger val à l'indice itop
  end empilerCoup;

  procedure depilerCoup(h : in out TV_Hist; itop : in out natural; val : out TR_Dep) is
  --{} => {si l'historique est vide, EX_PILE_VIDE est déclenchée, sinon le dernier élément
  --de l'historique a été enlevé, val contient sa valeur, itop est l'indice du dernier élément}
  begin
    if histoVide(h,itop) then
      raise EX_HIST_VIDE;
    else
      val := h(itop);
      itop := itop-1;
    end if;
  end depilerCoup;
end p_historique;

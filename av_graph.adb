with p_virus,p_vue_graph,text_io; use p_virus,p_vue_graph,text_io;
-- use p_vue_graph.p_fenbase,p_vue_graph.Forms;
with p_fenbase,Forms; use p_fenbase,Forms;
use p_virus.p_int_io,p_virus.p_piece_io;


procedure av_graph is
  FJeu,FMenu,FAide,FVic,FPopup : TR_Fenetre;
  Grille : TV_Grille;
  Pieces : TV_Pieces;
  f : p_piece_io.file_type;
  nivSelect : natural;
  pseudo : string(1..7) := (others => ' ');-- contraint par le type TR_Score

begin -- av_graph
  InitPartie(grille,pieces);-- Initialisation de la grille vide
  open(f,in_file,"Parties");-- On ouvre le fichier de configurations
  InitialiserFenetres;

  CreeFenMenu(FMenu,520,600);
  CreeFenGrille(FJeu,1000,640,75);
  CreeFenAide(FAide,400,600);
  CreeFenVic(FVic,400,650);
  CreeFenPopup(FPopup,400,150);

  MontrerFenetre(FMenu);
  -- LancerVic(FVic,tmp,42,377,6.47229E+01);
  ----------------- BOUCLE PRINCIPALE DU MENU -----------------------------------
  loop
    declare
      -- Attendre un bouton et ranger son nom dans btClique, déclaré dynamiquement
      btClique : string := AttendreBouton(FMenu);
    begin
      -- put_line("Bouton cliqué : " & btClique);-- debug

      if btClique = "btAide" then-- Si le bouton cliqué est Aide,
        LancerAide(FMenu,FAide);-- alors afficher fenêtre d'aide

      elsif btClique(1..2)="lv" then --sinon si le bouton cliqué est un niveau,
        -- Parser le nom du bouton pour avoir le numéro du niveau,
        nivSelect := integer'value(btClique(4..btClique'last));
        CacherFenetre(FMenu);-- Cacher la fenêtre de menu,
        MontrerFenetre(FJeu);--Afficher la fenêtre de jeu

        -- FONCTION PRINCIPALE DU JEU EN LUI-MÊME et de la fenêtre associée ---
        -- > Grille et Pieces nécessaires pour le déroulement du jeu ;
        -- > nivSelect et fichier f nécessaires pour configurer la grille
        -- avec le niveau spécifié ;
        -- > FVic et FPopup nécessaires car appelées par le jeu lui-même :
        -- fenêtre de victoire et fenêtre de confirmation pour abandonner.
        LancerPartie(FJeu,FVic,FPopup,Grille,Pieces,nivSelect,f,pseudo);
        -- Une fois fini, cacher la fenêtre de jeu et montrer le menu.
        CacherFenetre(FJeu);
        MontrerFenetre(FMenu);

      elsif btClique = "champPseudo" then-- sinon si le champ pseudo a été utilisé,
        --simplement actualiser la variable pseudo (chaîne de longueur fixe)

        pseudo := (others => ' ');-- Init avec des espaces
        declare
          s : string := ConsulterContenu(FMenu,"champPseudo");
        begin
          if s'last-s'first+1 > pseudo'last then-- Si s est plus long que pseudo,
            -- Remplir pseudo avec s en coupant la fin
            pseudo := s(s'first..s'first + pseudo'last-1);
          else
            -- sinon remplir tout ou partie de pseudo avec s
            pseudo(1..s'last - s'first +1) := s;
          end if;
        end;
        put(pseudo);
        put_line("|");
      end if;

      exit when btClique="btQuitMenu";
    end;-- fin 1re déclaration dynamique (menu)
  end loop;
  ---------------- BOUCLE PRINCIPALE DU MENU ---------------------------------

end av_graph;

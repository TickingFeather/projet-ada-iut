with text_io; use text_io;
with sequential_io;

package p_score is

  type TR_Score is record
    nbconfig : positive;
    pseudo : String(1..7);
    nbCoup : positive;
    temps : float;
  end record;

  package p_score_io is new sequential_io(TR_Score); use p_score_io;

  type TV_Score is array(positive range <>) of TR_Score;

  function nbElem(f : in p_score_io.file_type) return natural;
    --{f ouvert, f = <>} => {résultat : nombre d'éléments dans f}

  function NbScoreConfig (f : in p_score_io.file_type;  nbconfig : in positive) return natural;
    --{f ouvert, f-=<>} => {resultat : nombre de score enregistrés pour la configuration nbconfig}

  function getScoreConfig (f : in p_score_io.file_type;  nbconfig : in positive; nbElemConf : in natural) return TV_Score;
    -- {f ouvert, f- = <>} => {resultat = TV_SCore qui contient les scores pour la configuration nbconfig}

  function InfStrict(S1,S2 : TR_Score) return boolean;
    -- {} => {vrai si S1 est strictement inférieur à S2 selon l'ordre (nbcoup,temps)}

  procedure AjouterScore (f :  in out p_score_io.file_type; nbconfig : in positive; pseudo : String; nbCoup : positive; temps : float);
    -- {f ouvert} => {Le score a été inséré dans f, selon l'ordre (coup,temps)}
end p_score;

-- p_graphbase.ads

package p_graphbase is

	--------------------- TYPE POINT ------------------------------------------------------

	type TR_point is record  
		x: integer;  
		y: integer;  
	end record;

	--------------------- GESTION DU CONTEXTE GRAPHIQUE -----------------------------------

	type     T_couleur    is  ( blanc, noir, rouge, vert, jaune, bleu, cyan, magenta, gris );
	type     T_aspect     is  ( continu , pointille );
	subtype  T_epaisseur  is  positive range 1..10;

	type TR_contexte is record
		coul : T_couleur  := noir;
		asp  : T_aspect   := continu;
		epais: T_epaisseur:= 1;
	end record;

	contexte_standard : constant TR_contexte := (noir, continu, 1);
		 
			
	--------------------- TRACE DE TRAITS -------------------------------------------------

	procedure tracer_droite  ( p_orig : in TR_point; p_dest : in TR_point;
					cont : in TR_contexte);
	--{Une fenêtre est ouverte} => {Un trait de p_orig à p_dest a été tracé dans la fenêtre
	--										  avec la couleur, l'aspect et l'épasisseur définis dans cont}

	--------------------- GESTION DE LA FENETRE GRAPHIQUE ---------------------------------

	procedure Initialisation;
	--{Ne doit pas avoir déjà été appelée} => {Après son appel, une fenêtre peut être ouverte}
		
	procedure ouvrir_fenetre (largeur : in positive; hauteur : in positive );
	--{} => {Une fenêtre de fond gris, de dimension (largeur, longueur) et disposant d'un bouton
	--			"Suite" est ouverte}
		
	procedure fermer_fenetre ;
	--{Une fenêtre est ouverte} => {La fenêtre est fermée}

	function  bouton_enfonce return boolean;
	--{Une fenêtre est ouverte} => {résultat = vrai si l'utilisateur a cliqué sur le bouton "Suite"}
		
	procedure Attendre_Clic;
	--{Une fenêtre est ouverte} => {attend que l'utilisateur ait cliqué sur le bouton "Suite"}

end p_graphbase;
